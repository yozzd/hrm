import Vue from 'vue';
import Unicon from 'vue-unicons/dist/vue-unicons-ssr.common';

import {
  uniAngleDown,
  uniCalender,
  uniEditAlt,
  uniPlus,
  uniTimes,
} from 'vue-unicons/src/icons';

Unicon.add([
  uniAngleDown,
  uniCalender,
  uniEditAlt,
  uniPlus,
  uniTimes,
]);

Vue.use(Unicon);
