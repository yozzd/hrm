import Vue from 'vue';
import {
  Button,
  DatePicker,
  Dropdown,
  FormModel,
  Input,
  Menu,
  Modal,
  Select,
  Table,
  Tabs,
} from 'ant-design-vue';

Vue.use(Button);
Vue.use(DatePicker);
Vue.use(Dropdown);
Vue.use(FormModel);
Vue.use(Input);
Vue.use(Menu);
Vue.use(Modal);
Vue.use(Select);
Vue.use(Table);
Vue.use(Tabs);
