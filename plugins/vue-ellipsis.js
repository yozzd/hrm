import Vue from 'vue';
import ellipsis from '@hyjiacan/vue-ellipsis';

Vue.use(ellipsis);
